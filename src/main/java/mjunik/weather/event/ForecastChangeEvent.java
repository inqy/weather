package mjunik.weather.event;

import java.util.List;

import mjunik.weather.data.Location;
import mjunik.weather.data.WeatherState;

import java.time.LocalDate;
import java.util.ArrayList;

public class ForecastChangeEvent extends DataEvent {

    private Location location;
    private LocalDate dateStamp;
    private List<WeatherState> forecast;
    
    public ForecastChangeEvent(LocalDate date, Location loc) {
        this.location = loc;
        this.dateStamp = date;
        this.forecast = new ArrayList<>();
    }
    
    public Location getLocation() {
        return location;
    }
    
    public void addEntry(WeatherState e) {
        this.forecast.add(e);
    }
    
    public List<WeatherState> getEntries() {
        return forecast;
    }
    
    public LocalDate getDate() {
        return this.dateStamp;
    }
    
    public String toString(){
        StringBuilder s = new StringBuilder("ForecastChangeEvent{");
        for(WeatherState e : forecast) {
            s.append(" (");
            s.append(e.hashCode());
            s.append(" --> ");
            s.append(e.toString());
            s.append(")");
        }
        s.append("}");
        return s.toString();
    }
}
