package mjunik.weather.event;

public final class NetworkRequestFinishedEvent extends Event {

	@Override
	public java.lang.String toString() {
		return "NetworkRequestFinishedEvent()";
	}

}
