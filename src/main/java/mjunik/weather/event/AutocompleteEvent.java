package mjunik.weather.event;

import java.util.ArrayList;
import java.util.List;

import mjunik.weather.data.Location;

public class AutocompleteEvent extends DataEvent {

    private List<Location> matches;
    
    public AutocompleteEvent() {
        this.matches = new ArrayList<>();
    }
    
    public void addLocation(Location loc) {
        this.matches.add(loc);
    }
    
    public List<Location> getLocations() {
        return matches;
    }
    
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("AutocompleteEvent { ");
        for(Location l : matches) {
            s.append(l.toString());
            s.append(", ");
        }
        s.append("}");
        return s.toString();
    }
}
