package mjunik.weather.data;

import java.time.LocalDateTime;

public class WeatherState {
    private String location;
    private String weather;
    private String desc;
    private int temp;
    private int pressure;
    private int humidity;
    private float windSpeed;
    private int windDirection;
    private int clouds;
    private LocalDateTime sunrise;
    private LocalDateTime sunset;
    private LocalDateTime timestamp;
    private LocalDateTime date;
    
    private boolean locationSet = false;
    private boolean weatherSet = false;
    private boolean descSet = false;
    private boolean tempSet = false;;
    private boolean pressureSet = false;
    private boolean humiditySet = false;
    private boolean windSpeedSet = false;
    private boolean windDirectionSet = false;
    private boolean cloudsSet = false;
    private boolean sunriseSet = false;
    private boolean sunsetSet = false;
    private boolean timestampSet = false;
    private boolean dateSet = false;
    
    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
        this.locationSet = true;
    }
    public String getWeather() {
        return weather;
    }
    public void setWeather(String weather) {
        this.weather = weather;
        this.weatherSet = true;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
        this.descSet = true;
    }
    public int getTemp() {
        return temp;
    }
    public void setTemp(int temp) {
        this.temp = temp;
        this.tempSet = true;
    }
    public int getPressure() {
        return pressure;
    }
    public void setPressure(int pressure) {
        this.pressure = pressure;
        this.pressureSet = true;
    }
    public int getHumidity() {
        return humidity;
    }
    public void setHumidity(int humidity) {
        this.humidity = humidity;
        this.humiditySet = true;
    }
    public float getWindSpeed() {
        return windSpeed;
    }
    public void setWindSpeed(float windSpeed) {
        this.windSpeed = windSpeed;
        this.windSpeedSet = true;
    }
    public int getWindDirection() {
        return windDirection;
    }
    public void setWindDirection(int windDirection) {
        this.windDirection = windDirection;
        this.windDirectionSet = true;
    }
    public int getClouds() {
        return clouds;
    }
    public void setClouds(int clouds) {
        this.clouds = clouds;
        this.cloudsSet = true;
    }
    public LocalDateTime getSunrise() {
        return sunrise;
    }
    public void setSunrise(LocalDateTime sunrise) {
        this.sunrise = sunrise;
        this.sunriseSet = true;
    }
    public LocalDateTime getSunset() {
        return sunset;
    }
    public void setSunset(LocalDateTime sunset) {
        this.sunset = sunset;
        this.sunsetSet = true;
    }
    public LocalDateTime getTimestamp() {
        return timestamp;
    }
    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
        this.timestampSet = true;
    }
    public LocalDateTime getDate() {
        return date;
    }
    public void setDate(LocalDateTime date) {
        this.date = date;
        this.dateSet = true;
    }
    public boolean isLocationSet() {
        return locationSet;
    }
    public boolean isWeatherSet() {
        return weatherSet;
    }
    public boolean isDescSet() {
        return descSet;
    }
    public boolean isTempSet() {
        return tempSet;
    }
    public boolean isPressureSet() {
        return pressureSet;
    }
    public boolean isHumiditySet() {
        return humiditySet;
    }
    public boolean isWindSpeedSet() {
        return windSpeedSet;
    }
    public boolean isWindDirectionSet() {
        return windDirectionSet;
    }
    public boolean isCloudsSet(){
        return cloudsSet;
    }
    public boolean isSunriseSet() {
        return sunriseSet;
    }
    public boolean isSunsetSet() {
        return sunsetSet;
    }
    public boolean isTimestampSet() {
        return timestampSet;
    }
    public boolean isDateSet() {
        return dateSet;
    }
    
}
