package mjunik.weather.data;

import java.time.LocalDateTime;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import mjunik.weather.event.WeatherChangeEvent;
import rx.Observable;

public class Weather {

    private StringProperty locationProperty;
    private StringProperty weatherProperty;
    private StringProperty descProperty;
    private IntegerProperty tempProperty;
    private IntegerProperty pressureProperty;
    private IntegerProperty humidityProperty;
    private FloatProperty windSpeedProperty;
    private IntegerProperty windDirectionProperty;
    private IntegerProperty cloudsProperty;
    private ObjectProperty<LocalDateTime> sunriseProperty;
    private ObjectProperty<LocalDateTime> sunsetProperty;
    private ObjectProperty<LocalDateTime> timestampProperty;
    private ObjectProperty<LocalDateTime> dateProperty;
    
    private boolean isLocationSet = false;
    private boolean isWeatherSet = false;
    private boolean isDescSet = false;
    private boolean isTempSet = false;;
    private boolean isPressureSet = false;
    private boolean isHumiditySet = false;
    private boolean isWindSpeedSet = false;
    private boolean isWindDirectionSet = false;
    private boolean isCloudsSet = false;
    private boolean isSunriseSet = false;
    private boolean isSunsetSet = false;;
    private boolean isTimestampSet = false;
    private boolean isDateSet = false;
    
    public StringProperty getLocationProperty() {
        return locationProperty;
    }

    public StringProperty getWeatherProperty() {
        return weatherProperty;
    }

    public StringProperty getDescProperty() {
        return descProperty;
    }

    public IntegerProperty getTempProperty() {
        return tempProperty;
    }

    public IntegerProperty getPressureProperty() {
        return pressureProperty;
    }

    public IntegerProperty getHumidityProperty() {
        return humidityProperty;
    }

    public FloatProperty getWindSpeedProperty() {
        return windSpeedProperty;
    }

    public IntegerProperty getWindDirectionProperty() {
        return windDirectionProperty;
    }
    
    public IntegerProperty getCloudsProperty() {
        return cloudsProperty;
    }

    public ObjectProperty<LocalDateTime> getSunriseProperty() {
        return sunriseProperty;
    }

    public ObjectProperty<LocalDateTime> getSunsetProperty() {
        return sunsetProperty;
    }
    
    public ObjectProperty<LocalDateTime> getTimestampProperty() {
        return timestampProperty;
    }
    
    public ObjectProperty<LocalDateTime> getDateProperty() {
        return dateProperty;
    }

    public boolean isLocationSet() {
        return isLocationSet;
    }

    public boolean isWeatherSet() {
        return isWeatherSet;
    }

    public boolean isDescSet() {
        return isDescSet;
    }

    public boolean isTempSet() {
        return isTempSet;
    }

    public boolean isPressureSet() {
        return isPressureSet;
    }

    public boolean isHumiditySet() {
        return isHumiditySet;
    }

    public boolean isWindSpeedSet() {
        return isWindSpeedSet;
    }

    public boolean isWindDirectionSet() {
        return isWindDirectionSet;
    }

    public boolean isCloudsSet() {
        return isCloudsSet;
    }

    public boolean isSunriseSet() {
        return isSunriseSet;
    }

    public boolean isSunsetSet() {
        return isSunsetSet;
    }

    public boolean isTimestampSet() {
        return isTimestampSet;
    }

    public boolean isDateSet() {
        return isDateSet;
    }

    public Weather() {
        locationProperty = new SimpleStringProperty("-");
        weatherProperty = new SimpleStringProperty("-");
        descProperty = new SimpleStringProperty("-");
        tempProperty = new SimpleIntegerProperty(-274);
        pressureProperty = new SimpleIntegerProperty(-1);
        humidityProperty = new SimpleIntegerProperty(-1);
        windSpeedProperty = new SimpleFloatProperty(-1.0f);
        windDirectionProperty = new SimpleIntegerProperty(-1);
        cloudsProperty = new SimpleIntegerProperty(-1); 
        
        sunriseProperty = new SimpleObjectProperty<LocalDateTime>();
        sunsetProperty = new SimpleObjectProperty<LocalDateTime>();
        timestampProperty = new SimpleObjectProperty<LocalDateTime>();
        dateProperty = new SimpleObjectProperty<LocalDateTime>();
    }
    
    public void setDataSource(Observable<WeatherChangeEvent> e) {
        e.map(event -> event.getState())
        .subscribe(this::updateFromWeatherState);
    }
    
    public void updateFromWeatherState(WeatherState state) {
        if(state.isLocationSet()) {
            this.isLocationSet = true;
            locationProperty.set(state.getLocation());
        }
        
        if(state.isWeatherSet()) {
            this.isWeatherSet = true;
            weatherProperty.set(state.getWeather());
        }
        
        if(state.isTempSet()) {
            this.isTempSet = true;
            tempProperty.set(state.getTemp());
        }
        
        if(state.isPressureSet()) {
            this.isPressureSet = true;
            pressureProperty.set(state.getPressure());
        }
        
        if(state.isHumiditySet()) {
            this.isHumiditySet = true;
            humidityProperty.set(state.getHumidity());
        }
        
        if(state.isWindSpeedSet()) {
            this.isWindSpeedSet = true;
            windSpeedProperty.set(state.getWindSpeed());
        }
        
        if(state.isWindDirectionSet()) {
            this.isWindDirectionSet = true;
            windDirectionProperty.set(state.getWindDirection());
        }

        if(state.isCloudsSet()) {
            this.isCloudsSet = true;
            cloudsProperty.set(state.getClouds());       
        }
        
        if(state.isSunriseSet()) {
            this.isSunriseSet = true;
            sunriseProperty.set(state.getSunrise());
        }
        
        if(state.isSunsetSet()) {
            this.isSunsetSet = true;
            sunsetProperty.set(state.getSunset());
        }
        
        if(state.isDescSet()) {
            this.isDescSet = true;
            descProperty.set(state.getDesc());
        }
        
        if(state.isTimestampSet()) {
            this.isTimestampSet = true;
            timestampProperty.set(state.getTimestamp());
        }
        
        if(state.isDateSet()) {
            this.isDateSet = true;
            dateProperty.set(state.getDate());
        }
    }
    
}
