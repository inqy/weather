package mjunik.weather.network;

import org.jsoup.Jsoup;

import io.reactivex.netty.RxNetty;
import mjunik.weather.event.Event;
import rx.Observable;
import rx.schedulers.Schedulers;

public class HtmlWebSource extends WebSource {

    HtmlWebSourceData source;
    
    public HtmlWebSource(HtmlWebSourceData source) {
        this.source = source;
    }
    
    @Override
    protected <T> Observable<? extends Event> makeRequest() {
        return RxNetty.createHttpRequest(JsonHelper.withJsonHeader(prepareHttpGETRequest(source.createURL())))
                .subscribeOn(Schedulers.io())
                .compose(this::unpackResponse)
                .map(s -> Jsoup.parse(s))
                .map(source::convertToEvents)
                .flatMap(list -> Observable.from(list));
    }

}
