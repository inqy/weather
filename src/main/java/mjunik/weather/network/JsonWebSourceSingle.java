package mjunik.weather.network;

import io.reactivex.netty.RxNetty;
import mjunik.weather.event.Event;
import rx.Observable;
import rx.schedulers.Schedulers;

public class JsonWebSourceSingle extends WebSourceSingle {

    JsonWebSourceData source;
    
    public JsonWebSourceSingle(JsonWebSourceData source) {
        this.source = source;
    }
    
    @Override
    protected <T> Observable<? extends Event> makeRequest() {
        return RxNetty.createHttpRequest(JsonHelper.withJsonHeader(prepareHttpGETRequest(source.createURL())))
                .subscribeOn(Schedulers.io())
                .compose(this::unpackResponse)
                .map(JsonHelper::parse)
                .map(source::convertToEvents)
                .flatMap(list -> Observable.from(list));
    }

}
