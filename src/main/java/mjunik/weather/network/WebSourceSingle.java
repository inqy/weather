package mjunik.weather.network;

import java.util.concurrent.TimeUnit;

import mjunik.weather.event.ErrorEvent;
import mjunik.weather.event.Event;
import mjunik.weather.event.NetworkRequestFinishedEvent;
import mjunik.weather.event.NetworkRequestIssuedEvent;
import rx.Observable;

public abstract class WebSourceSingle extends WebSource {
    
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(WebSourceSingle.class);
    private static final int TIMEOUT = 20;
    
    @Override
    public Observable<? extends Event> dataSourceStream() {
        return Observable.concat(
                Observable.just(new NetworkRequestIssuedEvent()),
                makeRequest().timeout(TIMEOUT, TimeUnit.SECONDS).doOnError(log::error)
                        .cast(Event.class).onErrorReturn(ErrorEvent::new), 
                Observable.just(new NetworkRequestFinishedEvent()));
    }
    
}
