package mjunik.weather.network;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import mjunik.weather.App;
import mjunik.weather.data.Location;
import mjunik.weather.data.WeatherState;
import mjunik.weather.event.Event;
import mjunik.weather.event.ForecastChangeEvent;
import mjunik.weather.network.JsonWebSourceDataOWMCurrent.QueryType;

public class JsonWebSourceDataOWMForecast implements JsonWebSourceData {
    
    private static final String API_KEY = "ba6fa49401aae42340fb5892773a6b15";
    private static final String BASE_URL = "http://api.openweathermap.org/data/2.5/forecast";
    
    private Location location;
    private QueryType queryType;
    private App app;
    
    public Location getLocation() {
        return location;
    }
    
    
    /*
     * EXPLANATION:
     * By design, this class along with all other data sources does not need the app parameter.
     * I have encountered a dreadful bug however: the refresh button refreshes all available 
     * weather sources. When a forecast is refreshed in the background, all the icons are 
     * rendered wrong. I haven't managed to fix this issue, I am suspecting there is something 
     * wrong with ikonli's FontIcon. I have managed to fix it by not refreshing the forecasts
     * in the background. It isn't elegant, but it works.
     */
    public JsonWebSourceDataOWMForecast(App app, Location location, QueryType type) {
        this.app = app;
        this.location = location;
        this.queryType = type;
    }
    
    @Override
    public String createURL() {
        StringBuilder s = new StringBuilder(BASE_URL);
        
        if(queryType == QueryType.ID) {
            s.append("?id=");
            s.append(location.getOwmId());
        } else if(queryType == QueryType.NAME) {
            s.append("?q=");
            s.append(location.getShortLocation());
        } else if(queryType == QueryType.LATLON) {
            s.append("?lat=");
            s.append(location.getLatitude());
            s.append("&lon=");
            s.append(location.getLongitude());
        }
        
        s.append("&APPID=");
        s.append(API_KEY);
        return s.toString();
    }
    
    private WeatherState parseWeatherObj(JsonObject jsonObject) {
        WeatherState state = new WeatherState();
        
        if(jsonObject.has("dt")) {
            state.setDate(LocalDateTime.ofInstant(Instant.ofEpochMilli(jsonObject.get("dt").getAsLong()*1000), ZoneId.systemDefault()));
        }
        
        if(jsonObject.has("main")) {
            JsonObject main = jsonObject.get("main").getAsJsonObject();
            
            if(main.has("temp")) {
                state.setTemp((int)(main.get("temp").getAsFloat()-273.15));
            }
            
            if(main.has("pressure")) {
                state.setPressure((int)(main.get("pressure").getAsFloat()));
            }
            
            if(main.has("humidity")) {
                state.setHumidity(main.get("humidity").getAsInt());
            }
        }
        
        if(jsonObject.has("wind")) {
            JsonObject wind = jsonObject.get("wind").getAsJsonObject();
            if(wind.has("speed")) {
                state.setWindSpeed(wind.get("speed").getAsFloat());
            }
            
            if(wind.has("deg")) {
                state.setWindDirection((int)(wind.get("deg").getAsFloat()));
            }
        }
        
        if(jsonObject.has("clouds")) {
            JsonObject clouds = jsonObject.get("clouds").getAsJsonObject();
            if(clouds.has("all")) {
                state.setClouds(clouds.get("all").getAsInt());
            }
        }
        
        if(jsonObject.has("weather")) {
            JsonObject weather = jsonObject.get("weather").getAsJsonArray().get(0).getAsJsonObject();
            if(weather.has("main")) {
                state.setWeather(weather.get("main").getAsString());
            }
            
            if(weather.has("description")) {
                state.setDesc(weather.get("description").getAsString());
            }
        }
        
        state.setTimestamp(LocalDateTime.now());
        
        return state;
    }
    
    @Override
    public List<Event> convertToEvents(JsonElement jsonElement) {
        if(!app.isInFocus(location)) {
            return new ArrayList<Event>();
        }
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        LinkedHashMap<LocalDate, ForecastChangeEvent> eventMap = new LinkedHashMap<LocalDate, ForecastChangeEvent>();
        
        if(jsonObject.has("list")) {
            JsonArray arr = jsonObject.get("list").getAsJsonArray();
            
            for(Iterator<JsonElement> it = arr.iterator(); it.hasNext();){
                JsonObject o = it.next().getAsJsonObject();
                WeatherState weatherState = parseWeatherObj(o);
                if(weatherState.isDateSet()) {
                    LocalDate date = weatherState.getDate().toLocalDate();
                    if(!eventMap.containsKey(date)){
                        eventMap.put(date, new ForecastChangeEvent(date, location));
                    } 
                    eventMap.get(date).addEntry(weatherState);
                }
                
            }
        }
        
        List<Event> eventList = new ArrayList<>();
        eventMap.forEach((date, e) -> {eventList.add(e);});
        
        return eventList;
    }

}
