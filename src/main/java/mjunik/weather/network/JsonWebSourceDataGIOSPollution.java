package mjunik.weather.network;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import mjunik.weather.event.Event;
import mjunik.weather.event.PollutionChangeEvent;

public class JsonWebSourceDataGIOSPollution implements JsonWebSourceData {

    private static final String URL = "http://powietrze.gios.gov.pl/pjp/current/getAQIDetailsList?param=AQI";
    private static final int STATION_ID = 544; //{530, 531, 544, 550};
    
    @Override
    public String createURL() {
        return URL;
    }

    @Override
    public List<Event> convertToEvents(JsonElement jsonElement) {
        List<Event> list = new ArrayList<>();
        JsonArray arr = jsonElement.getAsJsonArray();

        for(int i = 0; i < arr.size(); i++) {
            JsonObject elem = arr.get(i).getAsJsonObject();
            
            if(elem.has("stationId")) {
                int stationId = elem.get("stationId").getAsInt();
                if(stationId == STATION_ID) {
                    if(elem.has("values")) {
                        JsonObject values = elem.get("values").getAsJsonObject();
                        double pm25 = -1;
                        double pm10 = -1;
                        
                        if(values.has("PM2.5")) {
                            pm25 = values.get("PM2.5").getAsDouble();
                        }
                        if(values.has("PM10")) {
                            pm10 = values.get("PM10").getAsDouble();
                        }
                        
                        list.add(new PollutionChangeEvent(pm25, pm10));
                    }  
                }
            }
        }
        
        return list;
    }

}
