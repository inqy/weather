package mjunik.weather.network;

import java.util.concurrent.TimeUnit;

import io.netty.buffer.ByteBuf;
import io.netty.util.CharsetUtil;
import io.reactivex.netty.protocol.http.client.HttpClientRequest;
import io.reactivex.netty.protocol.http.client.HttpClientResponse;
import mjunik.weather.event.ErrorEvent;
import mjunik.weather.event.Event;
import mjunik.weather.event.EventStream;
import mjunik.weather.event.NetworkRequestFinishedEvent;
import mjunik.weather.event.NetworkRequestIssuedEvent;
import mjunik.weather.event.RefreshRequestEvent;
import rx.Observable;
import rx.schedulers.Schedulers;

public abstract class WebSource {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(WebSource.class);
    
    private static final int POLL_INTERVAL = 600;
    private static final int INITIAL_DELAY = 2;
    private static final int TIMEOUT = 20;
    
    public WebSource() {
        
    }
    
    public Observable<? extends Event> dataSourceStream() {
        return fixedIntervalStream()
                .compose(this::wrapRequest)
                .mergeWith(
                        EventStream.getInstance()
                        .eventsInIO()
                        .ofType(RefreshRequestEvent.class)
                        .compose(this::wrapRequest)
               );
    }
    
    private Observable<Long> fixedIntervalStream() {
        return Observable.interval(INITIAL_DELAY, POLL_INTERVAL, TimeUnit.SECONDS, Schedulers.io());
    }
    
    protected abstract <T> Observable<? extends Event> makeRequest();
    
    protected HttpClientRequest<ByteBuf> prepareHttpGETRequest(String url) {
        return HttpClientRequest.createGet(url);
    }

    protected <T> Observable<String> unpackResponse(Observable<HttpClientResponse<ByteBuf>> responseObservable) {
        return responseObservable.flatMap(HttpClientResponse::getContent)
                .map(buffer -> buffer.toString(CharsetUtil.UTF_8));
    }
    
    private <T> Observable<Event> wrapRequest(Observable<T> observable) {
        return observable.flatMap(
                something -> Observable.concat(
                    Observable.just(new NetworkRequestIssuedEvent()),
                    makeRequest().timeout(TIMEOUT, TimeUnit.SECONDS).doOnError(log::error)
                            .cast(Event.class).onErrorReturn(ErrorEvent::new), 
                    Observable.just(new NetworkRequestFinishedEvent())
                )
        );
    }

}
