package mjunik.weather.network;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import mjunik.weather.data.Location;
import mjunik.weather.data.WeatherState;
import mjunik.weather.event.Event;
import mjunik.weather.event.WeatherChangeEvent;

public class HtmlWebSourceDataMeteoCurrent implements HtmlWebSourceData {

    private static final String URL = "http://www.meteo.waw.pl/";
    
    private Location location;
    
    public Location getLocationId() {
        return location;
    }

    public HtmlWebSourceDataMeteoCurrent(Location location) {
        this.location = location;
    }
    
    @Override
    public String createURL() {
        return URL;
    }

    @Override
    public List<Event> convertToEvents(Document doc) {
        List<Event> list = new ArrayList<>();
        
        Element temp = doc.getElementById("PARAM_TA");
        Element humidity = doc.getElementById("PARAM_RH");
        Element pressure = doc.getElementById("PARAM_PR");
        Element windSpeed = doc.getElementById("PARAM_WV");
        Element windDir = doc.getElementById("PARAM_WD");
        Element date = doc.getElementById("PARAM_LDATE");

        WeatherState state = new WeatherState();
        state.setTemp((int)Float.parseFloat(temp.html().replaceFirst(",", ".")));
        state.setHumidity((int)Float.parseFloat(humidity.html().replaceFirst(",", ".")));
        state.setPressure((int)Float.parseFloat(pressure.html().replaceFirst(",", ".")));
        state.setWindSpeed(Float.parseFloat(windSpeed.html().replaceFirst(",", ".")));
        state.setWindDirection((int)Float.parseFloat(windDir.html().replaceFirst(",", ".")));
        state.setDate(LocalDateTime.parse(date.html(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        state.setTimestamp(LocalDateTime.now());
        
        list.add(new WeatherChangeEvent(location, state, this.getClass()));
        return list;
    }

}
