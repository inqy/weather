package mjunik.weather.control;

import java.util.Collections;
import java.time.LocalDateTime;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class IconPicker {

    //@TODO: I should definitely load this monstrosity from a file...
    
    private static Map<String, String> timeIcons = 
        Collections.unmodifiableMap(Stream.of(
            new SimpleEntry<>("thunderstorm with light rain", "thunderstorm"),
            new SimpleEntry<>("thunderstorm with rain", "thunderstorm"),
            new SimpleEntry<>("thunderstorm with heavy rain", "thunderstorm"),
            new SimpleEntry<>("light thunderstorm", "lightning"),
            new SimpleEntry<>("thunderstorm", "lightning"),
            new SimpleEntry<>("heavy thunderstorm", "lightning"),
            new SimpleEntry<>("ragged thunderstorm", "lightning"),
            new SimpleEntry<>("thunderstorm with light drizzle", "storm-showers"),
            new SimpleEntry<>("thunderstorm with drizzle", "storm-showers"),
            new SimpleEntry<>("thunderstorm with heavy drizzle", "storm-showers"),
            new SimpleEntry<>("light intensity drizzle", "showers"),
            new SimpleEntry<>("drizzle", "rain-mix"),
            new SimpleEntry<>("heavy intensity drizzle", "rain"),
            new SimpleEntry<>("light intensity drizzle rain", "showers"),
            new SimpleEntry<>("drizzle rain", "rain-mix"),
            new SimpleEntry<>("heavy intensity drizzle rain", "rain"),
            new SimpleEntry<>("shower rain and drizzle", "rain-mix"),
            new SimpleEntry<>("heavy shower rain and drizzle", "rain"),
            new SimpleEntry<>("shower drizzle", "rain-mix"),
            new SimpleEntry<>("light rain", "showers"),
            new SimpleEntry<>("moderate rain", "rain-mix"),
            new SimpleEntry<>("heavy intensity rain", "rain"),
            new SimpleEntry<>("very heavy rain", "rain"),
            new SimpleEntry<>("extreme rain", "rain"),
            new SimpleEntry<>("freezing rain", "hail"),
            new SimpleEntry<>("light intensity shower rain", "showers"),
            new SimpleEntry<>("shower rain", "rain-mix"),
            new SimpleEntry<>("heavy intensity shower rain", "rain"),
            new SimpleEntry<>("ragged shower rain", "rain-mix"),
            new SimpleEntry<>("light snow", "snow"),
            new SimpleEntry<>("snow", "snow"),
            new SimpleEntry<>("heavy snow", "snow"),
            new SimpleEntry<>("sleet", "sleet"),
            new SimpleEntry<>("shower sleet", "sleet"),
            new SimpleEntry<>("light rain and snow", "snow"),
            new SimpleEntry<>("rain and snow", "snow"),
            new SimpleEntry<>("light shower snow", "snow"),
            new SimpleEntry<>("shower snow", "snow"),
            new SimpleEntry<>("heavy shower snow", "snow"),
            new SimpleEntry<>("clear sky", "clear"), // special.
            new SimpleEntry<>("few clouds", "partly-cloudy"),
            new SimpleEntry<>("scattered clouds", "cloudy"),
            new SimpleEntry<>("broken clouds", "cloudy")
        ).collect(Collectors.toMap((e) -> e.getKey(), (e) -> e.getValue())));
    
    private static Map<String, String> timelessIcons = 
        Collections.unmodifiableMap(Stream.of(
            new SimpleEntry<>("mist", "fog"),
            new SimpleEntry<>("smoke", "smoke"),
            new SimpleEntry<>("haze", "smog"),
            new SimpleEntry<>("sand, dust whirls", "sandstorm"),
            new SimpleEntry<>("fog", "fog"),
            new SimpleEntry<>("sand", "dust"),
            new SimpleEntry<>("dust", "dust"),
            new SimpleEntry<>("volcanic ash", "volcano"),
            new SimpleEntry<>("squalls", "rain"),
            new SimpleEntry<>("overcast clouds", "cloudy"), 
            new SimpleEntry<>("tornado", "tornado"),
            new SimpleEntry<>("tropical storm", "thunderstorm"),
            new SimpleEntry<>("hurricane", "hurricane"),
            new SimpleEntry<>("cold", "snowflake-cold"),
            new SimpleEntry<>("hot", "hot"), 
            new SimpleEntry<>("windy", "windy"),
            new SimpleEntry<>("hail", "hail")
        ).collect(Collectors.toMap((e) -> e.getKey(), (e) -> e.getValue())));
    
    
    public static String getIconId(LocalDateTime time, LocalDateTime sunrise, LocalDateTime sunset, String weatherDesc) {
        boolean isDay = time.isAfter(sunrise) ? time.isBefore(sunset) : false;
        String iconId = "";
        if(timeIcons.containsKey(weatherDesc)) {
            iconId = timeIcons.get(weatherDesc);
            if(iconId == "clear") {
                if(isDay){
                    iconId = "day-sunny";
                } else {
                    iconId = "night-" + iconId;
                }
            } else if(iconId == "partly-cloudy") {
                if(isDay){
                    iconId = "night-" + iconId;
                } else {
                    iconId = "night-alt" + iconId;
                }
            } else {
                if(isDay){
                    iconId = "day-" + iconId; 
                } else {
                    iconId = "night-alt-" + iconId;
                }
            }
            
        } else if(timelessIcons.containsKey(weatherDesc)) {
            iconId = timelessIcons.get(weatherDesc);
        } else {
            iconId = "na";
        }
        
        iconId = "wi-" + iconId;
        
        return iconId;
    }
    
    public static String getIconId(String weatherDesc) {
        String iconId = "";
        if(timeIcons.containsKey(weatherDesc)) {
            iconId = timeIcons.get(weatherDesc);
            if(iconId == "clear") {
                iconId = "day-sunny";
            } else if(iconId == "partly-cloudy") {
                iconId = "night-" + iconId; 
            } else { 
                iconId = "day-" + iconId;
            }
            
        } else if(timelessIcons.containsKey(weatherDesc)) {
            iconId = timelessIcons.get(weatherDesc);
        } else {
            iconId = "na";
        }
        
        iconId = "wi-" + iconId;
        
        return iconId;
    }
    
}