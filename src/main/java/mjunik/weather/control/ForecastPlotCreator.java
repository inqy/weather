package mjunik.weather.control;

import java.util.List;

import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.util.StringConverter;
import mjunik.weather.data.WeatherState;

public class ForecastPlotCreator {

    private List<WeatherState> list;
    
    public ForecastPlotCreator(List<WeatherState> list) {
        this.list = list;
    }
    
    
    public AreaChart<Number, Number> createPlot() throws Exception {
        if(list.isEmpty()) {
            throw new Exception("Empty weather data.");
        }
        
        NumberAxis xAxis = new NumberAxis(list.get(0).getDate().getHour(),list.get(list.size()-1).getDate().getHour(),3);
        xAxis.setMinorTickCount(3);
        NumberAxis yAxis = new NumberAxis();
        yAxis.tickUnitProperty().set(5);
        yAxis.setMinorTickCount(0);
        xAxis.tickLabelFormatterProperty().set(new StringConverter<Number>() {
            @Override
            public String toString(Number number) {
                String s = String.valueOf(number.intValue());
                return s.length() == 1? "0"+s+":00" : s+":00";
            }

            @Override
            public Number fromString(String string) {
                return Integer.parseInt(string.split(":")[0]);
            }
            
        });
        
        AreaChart<Number,Number> chart = new AreaChart<Number,Number>(xAxis,yAxis);
        yAxis.forceZeroInRangeProperty().set(true);
        
        chart.getStyleClass().add("hourlyChart");
        chart.setLegendVisible(false);
        chart.setCreateSymbols(false);
        
        XYChart.Series<Number, Number> series = new XYChart.Series<Number, Number>();    
        for(WeatherState w : list) {
            Number time = w.getDate().getHour();
            Number value = w.getTemp();
            XYChart.Data<Number, Number> datum = new XYChart.Data<Number, Number>(time, value);
            series.getData().add(datum);
        }
          
        chart.getData().add(series);
        return chart;
    }
    
    
    
}
