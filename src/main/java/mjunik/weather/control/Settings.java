package mjunik.weather.control;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import mjunik.weather.event.EventStream;
import mjunik.weather.event.SettingsRequestEvent;
import mjunik.weather.exception.LoadingFXMLFileException;
import rx.observables.JavaFxObservable;

public class Settings extends VBox {

    private static final String FXML_TEMPLATE = "/fxml/Settings.fxml";
    
    @FXML
    private Button backButton;
    
    public Settings () {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(FXML_TEMPLATE));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            LoadingFXMLFileException e = new LoadingFXMLFileException(FXML_TEMPLATE);
            throw e;
        }
        
        EventStream.joinStream(JavaFxObservable.actionEventsOf(backButton).map(e -> new SettingsRequestEvent()));
     }

}
