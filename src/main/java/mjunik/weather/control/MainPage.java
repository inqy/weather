package mjunik.weather.control;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.MessageFormat;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javafx.animation.RotateTransition;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.util.Duration;
import mjunik.weather.App;
import mjunik.weather.data.Location;
import mjunik.weather.data.Weather;
import mjunik.weather.event.AddLocationRequestEvent;
import mjunik.weather.event.AutocompleteEvent;
import mjunik.weather.event.ErrorEvent;
import mjunik.weather.event.Event;
import mjunik.weather.event.EventStream;
import mjunik.weather.event.ForecastChangeEvent;
import mjunik.weather.event.NetworkRequestFinishedEvent;
import mjunik.weather.event.NetworkRequestIssuedEvent;
import mjunik.weather.event.PollutionChangeEvent;
import mjunik.weather.event.RefreshRequestEvent;
import mjunik.weather.event.SettingsRequestEvent;
import mjunik.weather.event.SourceSwitchRequestEvent;
import mjunik.weather.exception.LoadingFXMLFileException;
import mjunik.weather.network.JsonWebSource;
import mjunik.weather.network.JsonWebSourceDataLocationLookup;
import mjunik.weather.network.JsonWebSourceDataOWMCurrent;
import mjunik.weather.network.JsonWebSourceDataOWMCurrent.QueryType;
import mjunik.weather.network.JsonWebSourceDataOWMForecast;
import mjunik.weather.network.JsonWebSourceSingle;
import rx.Observable;
import rx.observables.JavaFxObservable;
import rx.schedulers.JavaFxScheduler;

public class MainPage extends BorderPane {
    
    private static final int ERROR_MSG_MAX_LENGTH = 400;
    private static final int ERROR_MSG_DURATION = 30;
    private static final String FXML_TEMPLATE = "/fxml/MainPage.fxml";
    
    private Map<Location, WeatherPane> locations;
    
    private ObjectProperty<WeatherPane> currentPane;
    
    @FXML
    private GridPane mainPane;
    
    @FXML
    private StackPane searchResultsContainer;
    
    @FXML
    private Button searchButton;
    
    @FXML
    private VBox locationContainer;

    @FXML
    private StackPane weatherPaneContainer;

    @FXML
    private Button settingsButton;
    
    @FXML
    private Button refreshButton;
    
    @FXML
    private TextField searchBar;
    
    @FXML
    private Node errorIcon;

    @FXML
    private Node workingIcon;
    
    private VBox searchResultsBox;
    
    private App app;
    
    public MainPage(App app) {
        this.app = app;
        currentPane = new SimpleObjectProperty<>();
        locations = new HashMap<>();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(FXML_TEMPLATE));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            LoadingFXMLFileException e = new LoadingFXMLFileException(FXML_TEMPLATE);
            throw e;
        }
    }
    
    public boolean isInFocus(Location loc) {
        return currentPane.isNotNull().get() && currentPane.getValue().getLocation().equals(loc);
    }
    
    private void initializeAutocomplete() {
        Observable<Event> autocomplete = JavaFxObservable.eventsOf(searchButton, MouseEvent.MOUSE_CLICKED)
        .filter(event -> searchBar.textProperty().get().length() >= 3)
        .map(event -> new JsonWebSourceSingle(new JsonWebSourceDataLocationLookup(searchBar.textProperty().get())))
        .flatMap(source -> source.dataSourceStream());
        
        EventStream.joinStream(autocomplete);
        
        autocomplete.ofType(AutocompleteEvent.class)
        .observeOn(JavaFxScheduler.getInstance())
        .map(event -> {
            createSearchResultsContainer();
            return event;
        })
        .map(event -> event.getLocations())
        .subscribe(this::addToSearchResults); 
    }
    
    private void createSearchResultsContainer() {
        if(searchResultsBox != null) {
            searchResultsContainer.getChildren().remove(searchResultsBox);
            searchResultsBox = null;
        }
        searchResultsBox = new VBox();
        searchResultsBox.setOpacity(0.8);
        searchResultsContainer.getChildren().add(searchResultsBox);
        StackPane.setMargin(searchResultsBox, new Insets(28, 0, 0, 0));
        
        JavaFxObservable.eventsOf(searchResultsBox, MouseEvent.MOUSE_ENTERED).subscribe(ev-> {
            searchResultsBox.getStyleClass().add("entered");
        });
        
        JavaFxObservable.eventsOf(searchResultsBox, MouseEvent.MOUSE_EXITED).subscribe(ev2 -> {
            if(searchResultsBox.getStyleClass().contains("entered")) {
                searchResultsContainer.getChildren().remove(searchResultsBox);
                searchResultsBox = null;
            }
        });
    }
    
    private void addToSearchResults(List<Location> locs) {
        ObservableList<Location> oLocs = FXCollections.observableArrayList();
        for(Location loc : locs) {
            oLocs.add(loc);
        }
        
        final ListView<Location> lv = new ListView<Location>(oLocs);
        if(searchResultsBox != null) {
            searchResultsBox.getChildren().add(lv);       
        }
        
        Observable<AddLocationRequestEvent> onClick = JavaFxObservable.eventsOf(lv, MouseEvent.MOUSE_CLICKED)
            .map(e -> new AddLocationRequestEvent(
                    lv.getSelectionModel().getSelectedItem(), 
                    new JsonWebSource(new JsonWebSourceDataOWMCurrent(
                            lv.getSelectionModel().getSelectedItem(), 
                            QueryType.LATLON)
                    ),
                    new JsonWebSource(new JsonWebSourceDataOWMForecast(
                            app,
                            lv.getSelectionModel().getSelectedItem(), 
                            QueryType.LATLON)
                    )
            ));
        
        EventStream.joinStream(onClick);
    }

    
    private void takeSourceSwitchEvent(SourceSwitchRequestEvent e) {
        changeLocation(e.getLocation());
    }
    
    @FXML
    private void initialize() {
        EventStream.joinStream(JavaFxObservable.eventsOf(refreshButton, MouseEvent.MOUSE_CLICKED).map(e -> new RefreshRequestEvent()));
        EventStream.joinStream(JavaFxObservable.eventsOf(settingsButton, MouseEvent.MOUSE_CLICKED).map(e -> new SettingsRequestEvent()));
        EventStream.getInstance().events().ofType(SourceSwitchRequestEvent.class).subscribe(this::takeSourceSwitchEvent);
        
        this.initializeAutocomplete();
        this.initializeBackground();
        this.getStylesheets().add("https://fonts.googleapis.com/css?family=Open+Sans:400,700,800");
        this.initializeTooltips();
        this.initializeStatus();
    }
    
    private void initializeBackground() {
        Color color = new Color(0.16796875d, 0.4609375d, 0.93359375d, 1.0d);
        BackgroundFill blue = new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY);
        Stop[] stops = new Stop[] { new Stop(0, new Color(1d, 1d, 1d, 0.3d)), new Stop(1, new Color(0d, 0d, 0d, 0.3d))};
        LinearGradient lg1 = new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, stops);
        BackgroundFill gradient = new BackgroundFill(lg1, null, null);
        Background background = new Background(new BackgroundFill[]{blue, gradient});
        this.setBackground(background);
    }
    
    private void initializeStatus() {
        Observable<Event> events = EventStream.getInstance().events();

        RotateTransition workingIconRotation = new RotateTransition(Duration.seconds(2.0), workingIcon);
        workingIconRotation.setFromAngle(0);
        workingIconRotation.setByAngle(360);
        workingIconRotation.setCycleCount(-1);
        workingIconRotation.play();
        
        workingIcon.visibleProperty()
                .bind(EventStream.binding(events.ofType(NetworkRequestIssuedEvent.class).map(e -> 1) 
                        .mergeWith(events.ofType(NetworkRequestFinishedEvent.class).map(e -> -1) 
                                .delay(2, TimeUnit.SECONDS, JavaFxScheduler.getInstance())) 
                        .scan(0, (x, y) -> x + y)
                        .map(v -> v > 0))
        );

        Observable<ErrorEvent> errors = events.ofType(ErrorEvent.class);
        errorIcon.visibleProperty()
        .bind(EventStream.onEvent(errors, true)
        .andOn(errors.throttleWithTimeout(ERROR_MSG_DURATION, TimeUnit.SECONDS, JavaFxScheduler.getInstance()),
                        false).toBinding());
    }
    
    private void initializeTooltips() {
        Tooltip.install(workingIcon, new Tooltip("Fetching data..."));

        Tooltip errorTooltip = new Tooltip();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        EventStream.getInstance().eventsInFx().ofType(ErrorEvent.class).subscribe(e -> {
            ByteArrayOutputStream ostream = new ByteArrayOutputStream();
            e.getCause().printStackTrace(new PrintStream(ostream));
            String details = new String(ostream.toByteArray());
            if (details.length() > ERROR_MSG_MAX_LENGTH) {
                details = details.substring(0, ERROR_MSG_MAX_LENGTH) + "\u2026"; 
            }

            errorTooltip.setText(MessageFormat.format("An error has occurred ({0}):\n{1}",
                    e.getTimestamp().format(formatter), details));
        });
        Tooltip.install(errorIcon, errorTooltip);
    }
    
    
    public void addLocation(Location location, Weather w, Observable<ForecastChangeEvent> f) {
        WeatherPane pane = new WeatherPane(location, w, f);
        locations.put(location, pane);
        weatherPaneContainer.getChildren().clear();
        weatherPaneContainer.getChildren().add(pane);
        currentPane.set(pane);
        
        LocationBox locationBox = new LocationBox(locations.size() == 1, location);
        locationContainer.getChildren().addAll(locationBox);
    }
    
    public void addLocation(Location location, Weather w, Observable<ForecastChangeEvent> f, Observable<PollutionChangeEvent> e) {
        addLocation(location, w, f);
        locations.get(location).setPollutionData(e);
    }
    
    
    public void changeLocation(Location loc) {
        if(locations.containsKey(loc)) {
            WeatherPane pane = locations.get(loc);
            weatherPaneContainer.getChildren().clear();
            weatherPaneContainer.getChildren().add(pane);
            currentPane.set(pane);
        }
    }

}
