package mjunik.weather.control;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import mjunik.weather.data.ForecastManager;
import mjunik.weather.data.Location;
import mjunik.weather.data.Weather;
import mjunik.weather.event.ForecastChangeEvent;
import mjunik.weather.event.PollutionChangeEvent;
import mjunik.weather.exception.LoadingFXMLFileException;
import rx.Observable;

public class WeatherPane extends GridPane {

    private static final String FXML_TEMPLATE ="/fxml/WeatherPane.fxml";
    
    private Location location;
    private Weather weather;
    private CurrentWeatherBox weatherBox;
    
    @FXML
    private StackPane currentContainer;
    
    @FXML
    private BorderPane forecastContainer;
    
    @FXML
    private StackPane hourlyContainer;
    
    
    public WeatherPane(Location location, Weather w, Observable<ForecastChangeEvent> f) {
        this.location = location;
        this.weather = w;
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(FXML_TEMPLATE));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);   
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            LoadingFXMLFileException e = new LoadingFXMLFileException(FXML_TEMPLATE);
            throw e;
        }
        
        setWeatherBox(w);
        if(f != null) {
            @SuppressWarnings("unused")
            ForecastManager forecastManager = new ForecastManager(forecastContainer, hourlyContainer, f);
        }    
    }
    
    public Location getLocation() {
        return location;
    }
    
    public Weather getWeather() {
        return weather;
    }
    
    private void setWeatherBox(Weather w) {
        weatherBox = new CurrentWeatherBox();
        weatherBox.setWeatherObj(w);
        currentContainer.getChildren().add(weatherBox);   
    }
    
    public void setPollutionData(Observable<PollutionChangeEvent> e) {
        if(weatherBox != null) {
            weatherBox.setPollutionDataSource(e);
        }
    }
}
