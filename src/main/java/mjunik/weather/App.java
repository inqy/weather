package mjunik.weather;

import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;
import mjunik.weather.control.MainPage;
import mjunik.weather.control.Settings;
import mjunik.weather.data.Location;
import mjunik.weather.data.Weather;
import mjunik.weather.event.AddLocationRequestEvent;
import mjunik.weather.event.Event;
import mjunik.weather.event.EventStream;
import mjunik.weather.event.ForecastChangeEvent;
import mjunik.weather.event.PollutionChangeEvent;
import mjunik.weather.event.SettingsRequestEvent;
import mjunik.weather.event.WeatherChangeEvent;
import mjunik.weather.network.HtmlWebSource;
import mjunik.weather.network.HtmlWebSourceDataMeteoCurrent;
import mjunik.weather.network.JsonWebSource;
import mjunik.weather.network.JsonWebSourceDataGIOSPollution;
import mjunik.weather.network.JsonWebSourceDataOWMCurrent;
import mjunik.weather.network.JsonWebSourceDataOWMCurrent.QueryType;
import mjunik.weather.network.JsonWebSourceDataOWMForecast;
import mjunik.weather.network.WebSource;
import rx.Observable;
import rx.Subscription;

/*
 * USAGE:
 * Click on the forecast panes in the middle to draw a temperature graph.
 * Put a location name or a part of it in the search bar and press the search button.
 * Click on one of the search results to add it to the list.
 * The two default Warsaw locations have different weather sources (owm and meteo).
 */
public class App extends Application
{
    private List<Subscription> sourceStreams = new ArrayList<Subscription>(); 
    
    private Stage stage;
    private MainPage mainPage;
    private List<Scene> scenes;
    
    public boolean isInFocus(Location loc) {
        if(mainPage != null) {
            return mainPage.isInFocus(loc);
        } else {
            return false;
        }
    }
    
    public void addLocation(Location loc, WebSource weatherSrc, WebSource forecastSrc) {
        Weather weather = new Weather();
        weather.setDataSource(EventStream.getInstance().events().ofType(WeatherChangeEvent.class)
                .filter(event -> event.getLocation().equals(loc)));
        
        mainPage.addLocation(loc, weather, EventStream.getInstance().events().ofType(ForecastChangeEvent.class)
                .filter(event -> event.getLocation().equals(loc)));
        
        sourceStreams.add(EventStream.joinStream(weatherSrc.dataSourceStream()));
        sourceStreams.add(EventStream.joinStream(forecastSrc.dataSourceStream()));
    }
    
    public void addDefaultLocation(Location loc, WebSource weatherSrc, WebSource forecastSrc, Observable<PollutionChangeEvent> e) {
        Weather weather = new Weather();
        weather.setDataSource(EventStream.getInstance().events().ofType(WeatherChangeEvent.class)
                .filter(event -> event.getLocation().equals(loc)));
        
        mainPage.addLocation(loc, weather, EventStream.getInstance().events().ofType(ForecastChangeEvent.class)
                .filter(event -> event.getLocation().equals(loc)), e);
        
        sourceStreams.add(EventStream.joinStream(weatherSrc.dataSourceStream()));
        if(forecastSrc != null) {
            sourceStreams.add(EventStream.joinStream(forecastSrc.dataSourceStream()));
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        mainPage = new MainPage(this);
        setupDefaultLocations();
        setupEventHandler();
     
        Scene mainScene = new Scene(mainPage, 1280, 720);
        primaryStage.setScene(mainScene);
        primaryStage.setTitle("Weather");
        primaryStage.setMinHeight(720);
        primaryStage.setMinWidth(1280);
        primaryStage.show();
        primaryStage.setResizable(false);
        
        this.stage = primaryStage;
        this.scenes = new ArrayList<>();
        this.scenes.add(mainScene);
        
        Settings settings = new Settings();
        Scene settingsScene = new Scene(settings, 1280, 720);
        scenes.add(settingsScene);
    }
    
    private void setupDefaultLocations() {
        Location defaultOwm = new Location("Warsaw, Poland", "Warsaw", "PL", "", "");
        defaultOwm.setOwmId("6695624");
        Location defaultMeteo = new Location("Warsaw, Poland (meteo)", "Warsaw", "PL", "", "");
        
        addDefaultLocation(defaultOwm, new JsonWebSource(new JsonWebSourceDataOWMCurrent(defaultOwm,QueryType.ID)),
                new JsonWebSource(new JsonWebSourceDataOWMForecast(this, defaultOwm, QueryType.ID)),
                EventStream.getInstance().events().ofType(PollutionChangeEvent.class));
        
        addDefaultLocation(defaultMeteo, new HtmlWebSource(new HtmlWebSourceDataMeteoCurrent(defaultMeteo)),
                null,
                EventStream.getInstance().events().ofType(PollutionChangeEvent.class));
        
        
        mainPage.changeLocation(defaultOwm);
        
        WebSource source =  new JsonWebSource(new JsonWebSourceDataGIOSPollution());
        sourceStreams.add(EventStream.joinStream(source.dataSourceStream()));
    }
    
    private void setupEventHandler() {
        Observable<Event> events = EventStream.getInstance().events();

        events.ofType(SettingsRequestEvent.class).subscribe(e -> onSettingsRequested());
        events.ofType(AddLocationRequestEvent.class).subscribe(e -> addLocation(e.getLocation(),e.getWeatherSource(), e.getForecastSource()));
    }
    
    private void onSettingsRequested() {
        if(stage.getScene().equals(scenes.get(0))) {
            stage.setScene(scenes.get(1));
        } else {
            stage.setScene(scenes.get(0));
        }  
    }
    
    public static void main( String[] args ) {
        Platform.setImplicitExit(true);
        
        launch(args);
    }
}
